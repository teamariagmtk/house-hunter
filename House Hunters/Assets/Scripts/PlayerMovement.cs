﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour {
    public static GameObject ins;
    public float acceleration;
    public float maxSpeed;
    private Rigidbody2D rig;
    
    // Use this for initialization
    void Start() {
        ins = gameObject;
        rig = gameObject.GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        move_player();

    }

    private void move_player()
    {
        if (Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.D))
        {
            if (Input.GetKey(KeyCode.A))
            {
                rig.velocity += new Vector2(-acceleration, 0);
            }
            if (Input.GetKey(KeyCode.D))
            {
                rig.velocity += new Vector2(acceleration, 0);
            }
        }
        else
            rig.velocity = new Vector2(0,rig.velocity.y);

        if (Input.GetKey(KeyCode.S) || Input.GetKey(KeyCode.W))
        {
            if (Input.GetKey(KeyCode.S))
            {
                rig.velocity += new Vector2(0, -acceleration);
            }
            if (Input.GetKey(KeyCode.W))
            {
                rig.velocity += new Vector2(0, acceleration);
            }
        }
        else
            rig.velocity = new Vector2(rig.velocity.x,0);
        if (Mathf.Abs(rig.velocity.x) > maxSpeed)
            rig.velocity = new Vector2(maxSpeed * rig.velocity.x / Mathf.Abs(rig.velocity.x), rig.velocity.y);
        if (Mathf.Abs(rig.velocity.y) > maxSpeed)
            rig.velocity = new Vector2(rig.velocity.x, maxSpeed * rig.velocity.y / Mathf.Abs(rig.velocity.y));

    }
}
