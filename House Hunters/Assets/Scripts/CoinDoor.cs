﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LockedDoor
{
    public GameObject obj;
    public int coinReq;
    public LockedDoor(GameObject obj_, int coinReq_)
    {
        obj = obj_;
        coinReq = coinReq_;
        
    }
}

public class CoinDoor : MonoBehaviour {
    public int requirement;

    // Use this for initialization
    void Start() {
        CoinController.ins.Doors.Add(new LockedDoor(gameObject, requirement));


    }

    // Update is called once per frame
    void Update() {

        
	}
}
