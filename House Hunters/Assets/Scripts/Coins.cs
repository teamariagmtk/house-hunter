﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Coins : MonoBehaviour {
    
    void OnTriggerEnter2D(Collider2D c)
    {
        CoinController.ins.GetCoin();
        gameObject.SetActive(false);
    }
}
