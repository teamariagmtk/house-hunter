﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoinController : MonoBehaviour {
    public static CoinController ins;
    public List<LockedDoor> Doors;
    public int Coin;
    // Use this for initialization
    void Start()
    {
        Doors = new List<LockedDoor>();
        ins = gameObject.GetComponent<CoinController>();
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void GetCoin()
    {
        Coin++;
        foreach (LockedDoor l in Doors)
        {
            if (l.coinReq <= Coin)
            {
                l.obj.SetActive(false);
            }
        }
    }
}
