﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WallDV : MonoBehaviour {
    public Sprite pic;
    public Sprite blank;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if (gameObject.transform.position.y > PlayerMovement.ins.transform.position.y) 
            GetComponent<SpriteRenderer>().sprite = pic;
        else
            GetComponent<SpriteRenderer>().sprite = blank;
    }
}
